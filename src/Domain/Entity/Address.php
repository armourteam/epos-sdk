<?php

declare(strict_types=1);

namespace ArmourDev\EposSDK\Domain\Entity;

class Address
{
    private function __construct(
        private string $address_line_1,
        private string $address_line_2,
        private string $city,
        private string $postcode,
    ) {
    }

    public static function create(
        string $address_line_1,
        string $address_line_2,
        string $city,
        string $postcode,
    ): self {
        return new self(
            address_line_1: $address_line_1,
            address_line_2: $address_line_2,
            city: $city,
            postcode: $postcode,
        );
    }

    /**
     * @return string
     */
    public function getAddressLine1(): string
    {
        return $this->address_line_1;
    }

    /**
     * @return string
     */
    public function getAddressLine2(): string
    {
        return $this->address_line_2;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getPostcode(): string
    {
        return $this->postcode;
    }

    /**
     * @return array
     */
    public function format(): array
    {
        return [
            "address_line_1" => $this->getAddressLine1(),
            "address_line_2" => $this->getAddressLine2(),
            "city" => $this->getCity(),
            "postcode" => $this->getPostcode(),
        ];
    }
}
