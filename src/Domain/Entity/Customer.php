<?php

declare(strict_types=1);

namespace ArmourDev\EposSDK\Domain\Entity;

class Customer
{
    private function __construct(
        private string $first_name,
        private string $last_name,
        private string $email,
        private string $phone,
    ) {
    }

    public static function create(
        string $first_name,
        string $last_name,
        string $email,
        string $phone,
    ): self {
        return new self(
            first_name: $first_name,
            last_name: $last_name,
            email: $email,
            phone: $phone,
        );
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->first_name;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->last_name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @return array
     */
    public function format(): array
    {
        return [
            'first_name'=> $this->getFirstName(),
            'last_name'=> $this->getLastName(),
            'email'=> $this->getEmail(),
            'phone'=> $this->getPhone(),
        ];
    }
}
