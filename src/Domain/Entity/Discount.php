<?php

declare(strict_types=1);

namespace ArmourDev\EposSDK\Domain\Entity;

class Discount
{
    private function __construct(
        private string $description,
        private float $total,
    ) {
    }

    public static function create(
        string $description,
        float $total,
    ): self {
        return new self(
            description: $description,
            total: $total,
        );
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return intval($this->total * 100);
    }

    /**
     * @return array
     */
    public function format(): array
    {
        return [
            'description' => $this->getDescription(),
            'total' => $this->getTotal(),
        ];
    }
}
