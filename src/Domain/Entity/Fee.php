<?php

declare(strict_types=1);

namespace ArmourDev\EposSDK\Domain\Entity;

class Fee
{
    private function __construct(
        private string $name,
        private float $price,
        private ?string $ref,
    ) {
    }

    public static function create(
        string $name,
        float $price,
        ?string $ref = null,
    ): self {
        return  new self(
            name: $name,
            price: $price,
            ref: $ref
        );
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return intval($this->price * 100);
    }

    /**
     * @return string|null
     */
    public function getRef(): ?string
    {
        return $this->ref ?: null;
    }

    /**
     * @return array
     */
    public function format(): array
    {
        return [
            'name' => $this->getName(),
            'price' => $this->getPrice(),
            'ref' => $this->getRef(),
        ];
    }
}
