<?php

declare(strict_types=1);

namespace ArmourDev\EposSDK\Domain\Entity;

class Item
{
    private array $notes;

    private function __construct(
        private int $id,
        private string $name,
        private int $quantity,
        private float $price,
    ) {
    }

    public static function create(
        int $id,
        string $name,
        int $quantity,
        float $price,
        ?array $comments = null
    ): self {
        $item = new self(
            id: $id,
            name: $name,
            quantity: $quantity,
            price: $price,
        );
    
        if ($comments) {
            $item->setNotes($comments);
        }

        return $item;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getPrice(): int
    {
        return intval($this->price * 100);
    }

    /**
     * @param array $notes
     */
    public function setNotes(array $notes): void
    {
        $this->notes = $notes;
    }

    /**
     * @return array
     */
    public function getNotes(): array
    {
        return $this->notes;
    }

    /**
     * @param Item ...$items
     * @return void
     */
    public function addItems(Item ...$items): void
    {
        empty($this->items) ? $this->items = $items : array_push($this->items, ...$items);
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return array
     */
    public function format(): array
    {
        $item = [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'quantity' => $this->getQuantity(),
            'price' => $this->getPrice(),
        ];

        if (isset($this->items) && $this->getItems() !== null) {
            $item['items'] = array_map(fn ($item) => $item->format(), $this->getItems());
        }

        if (isset($this->notes) && $this->getNotes() !== null) {
            $item['notes'] = $this->getNotes();
        }

        return $item;
    }
}
