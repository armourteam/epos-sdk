<?php

declare(strict_types=1);

namespace ArmourDev\EposSDK\Domain\Entity;

use Exception;

class Order
{
    public OrderType $type;

    public array $items;

    public Payment $payment;

    public Customer $customer;

    public array $discounts;

    public Address $address;

    public array $customer_notes = [];

    private function __construct(
        private int $id,
        private int $restaurant_id,
        private string $epos_restaurant_id,
        private string $ready_at
    ) {
    }

    public static function create(
        int $id,
        int $restaurant_id,
        string $epos_restaurant_id,
        string $ready_at
    ): self {
        return new self(
            id: $id,
            restaurant_id: $restaurant_id,
            epos_restaurant_id: $epos_restaurant_id,
            ready_at: $ready_at,
        );
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getRestaurantId(): int
    {
        return $this->restaurant_id;
    }

    /**
     * @return string
     */
    public function getEposRestaurantId(): string
    {
        return $this->epos_restaurant_id;
    }

    /**
     * @return string
     */
    public function getReadyAt(): string
    {
        return $this->ready_at;
    }

    /**
     * @throws Exception
     */
    public function addOrderType(OrderType $type): void
    {
        $this->type = $type;
    }

    /**
     * @return OrderType
     */
    public function getOrderType(): OrderType
    {
        return $this->type;
    }

    /**
     * @param Item ...$items
     * @return void
     */
    public function addItems(Item ...$items): void
    {
        empty($this->items) ? $this->items = $items : array_push($this->items, ...$items);
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param Payment $payment
     * @return void
     */
    public function addPayment(Payment $payment): void
    {
        $this->payment = $payment;
    }

    /**
     * @return Payment
     */
    public function getPayment(): Payment
    {
        return $this->payment;
    }

    /**
     * @param Customer $customer
     * @return void
     */
    public function addCustomer(Customer $customer): void
    {
        $this->customer = $customer;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @param Discount ...$discounts
     * @return void
     */
    public function addDiscounts(Discount ...$discounts): void
    {
        empty($this->discounts) ? $this->discounts = $discounts : array_push($this->discounts, ...$discounts);
    }

    /**
     * @return array
     */
    public function getDiscounts(): array
    {
        return $this->discounts;
    }

    /**
     * @param Address $address
     * @return void
     */
    public function addAddress(Address $address): void
    {
        $this->address = $address;
    }

    /**
     * @return Address
     */
    public function getAddress(): Address
    {
        return $this->address;
    }

    /**
     * @param array $notes
     * @return void
     */
    public function addCustomerNotes(array $notes): void
    {
        foreach ($notes as $note) {
            $this->customer_notes[] = $note['text'];
        }
    }

    /**
     * @return array
     */
    public function getCustomerNotes(): array
    {
        return $this->customer_notes;
    }

    /**
     * @return array
     */
    public function format(): array
    {
        return [
            'order' => [
                'id' => $this->getId(),
                'restaurant_id' => $this->getRestaurantId(),
                'epos_restaurant_id' => $this->getEposRestaurantId(),
                'ready_at' => $this->getReadyAt(),
                'type' => $this->getOrderType()->format(),
                'items' => array_map(fn ($item) => $item->format(), $this->getItems()),
                'customer' => $this->getCustomer()->format(),
                'address' => $this->getAddress()->format(),
                'payment' => $this->getPayment()->format(),
                'discounts' => array_map(fn ($discount) => $discount->format(), $this->getDiscounts()),
                'customer_notes' => $this->getCustomerNotes(),
            ],
        ];
    }

    /**
     * @throws Exception
     */
    public function validate(): void
    {
        $this->validateOrderType();
        $this->validateOrderItems();
        $this->validateOrderCustomer();
        $this->validateOrderAddress();
        $this->validateOrderPayment();
        $this->validateOrderDiscounts();
    }

    /**
     * @throws Exception
     */
    public function validateOrderType(): void
    {
        if (! isset($this->type)) {
            throw new Exception("order type not defined");
        }
    }

    /**
     * @throws Exception
     */
    public function validateOrderItems(): void
    {
        if (! isset($this->items)) {
            throw new Exception("items not defined");
        }
    }

    /**
     * @throws Exception
     */
    public function validateOrderCustomer(): void
    {
        if (! isset($this->customer)) {
            throw new Exception("customer not defined");
        }
    }

    /**
     * @throws Exception
     */
    public function validateOrderAddress(): void
    {
        if (! isset($this->address)) {
            throw new Exception("address not defined");
        }
    }

    /**
     * @throws Exception
     */
    public function validateOrderPayment(): void
    {
        if (! isset($this->payment)) {
            throw new Exception("payment not defined");
        }
    }

    /**
     * @throws Exception
     */
    public function validateOrderDiscounts(): void
    {
        if (! isset($this->discounts)) {
            throw new Exception("discount not defined");
        }
    }
}
