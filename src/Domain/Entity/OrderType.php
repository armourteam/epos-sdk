<?php

declare(strict_types=1);

namespace ArmourDev\EposSDK\Domain\Entity;

use ArmourDev\EposSDK\Domain\Enum\OrderTypeEnum;
use Exception;

class OrderType
{
    /**
     * @throws Exception
     */
    private function __construct(
        private string $description,
        private ?string $ref,
        private ?string $table_number = null,
    ) {
        $this->setTableNumber($this->table_number);
    }

    /**
     * @throws Exception
     */
    public static function create(
        string $description,
        ?string $ref = null,
        ?string $table_number = null,
    ): self {
        return new self(
            description: $description,
            ref: $ref,
            table_number: $table_number,
        );
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string|null
     */
    public function getRef(): ?string
    {
        return $this->ref;
    }

    /**
     * @return string|null
     */
    public function getTableNumber(): ?string
    {
        return $this->table_number;
    }

    /**
     * @param  string|null  $table_number
     *
     * @throws Exception
     */
    public function setTableNumber(?string $table_number): void
    {
        if ($this->description == OrderTypeEnum::TABLE && ! $this->table_number) {
            throw new Exception('table number should be defined');
        }

        $this->table_number = $this->description == OrderTypeEnum::TABLE ? $table_number : null;
    }

    /**
     * @return array
     */
    public function format(): array
    {
        return [
            'description' => $this->getDescription(),
            'ref' => $this->getRef(),
            'table_number' => $this->getTableNumber(),
        ];
    }
}
