<?php

declare(strict_types=1);

namespace ArmourDev\EposSDK\Domain\Entity;

class Payment
{
    private array $fees;

    private function __construct(
        private float $total,
        private bool $status,
        private string $method,
    ) {
    }

    public static function create(
        float $total,
        bool $status,
        string $method,
    ): self {
        return new self(
            total: $total,
            status: $status,
            method: $method,
        );
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return intval($this->total * 100);
    }

    /**
     * @return bool
     */
    public function getStatus(): bool
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return array
     */
    public function getFees(): array
    {
        return $this->fees;
    }

    /**
     * @param Fee ...$fees
     * @return void
     */
    public function setFees(Fee ...$fees): void
    {
        $this->fees = $fees;
    }

    /**
     * @return array
     */
    public function format(): array
    {
        $payment = [
            'total' => $this->getTotal(),
            'status' => $this->getStatus(),
            'method' => $this->getMethod(),
        ];

        if (isset($this->fees) && $this->getFees() !== null) {
            $payment['fees'] = array_map(fn ($fee) => $fee->format(), $this->getFees());
        }

        return $payment;
    }
}
