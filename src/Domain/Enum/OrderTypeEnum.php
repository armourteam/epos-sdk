<?php

declare(strict_types=1);

namespace ArmourDev\EposSDK\Domain\Enum;

class OrderTypeEnum
{
    const COLLECTION = 'collection';

    const DELIVERY = 'delivery';

    const TABLE = 'table';
}
