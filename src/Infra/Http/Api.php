<?php

declare(strict_types=1);

namespace ArmourDev\EposSDK\Infra\Http;

use ArmourDev\EposSDK\Domain\Entity\Order;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class Api
{
    private Client $client;

    public function __construct(
        private string $apiUrl,
        private Header $header
    ) {
        $this->client = new Client([
            'base_uri' => $this->apiUrl,
            'headers' => $this->header->format(),
        ]);
    }

    public static function create(
        string $apiUrl,
        Header $header
    ): self {
        return new self(
            apiUrl: $apiUrl,
            header: $header
        );
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @throws GuzzleException
     */
    public function getProducts()
    {
        $response = $this->getClient()->request(
            method: "GET",
            uri: "v1/products"
        );

        return $response->getBody()->getContents();
    }

    /**
     * @throws Exception
     * @throws GuzzleException
     */
    public function createOrder(Order $order)
    {
        $order->validate();

        $response = $this->getClient()->request(
            method: "POST",
            uri: "v1/orders",
            options: ['body' => json_encode($order->format())]
        );

        return $response->getBody()->getContents();
    }
}
