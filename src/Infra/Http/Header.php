<?php

declare(strict_types=1);

namespace ArmourDev\EposSDK\Infra\Http;

class Header
{
    private function __construct(
        private string $access_token,
        private string $resource,
        private string $api_key
    ) {
    }

    public static function create(
        string $access_token,
        string $resource,
        string $api_key
    ): self {
        return new self(
            $access_token,
            $resource,
            $api_key,
        );
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->access_token;
    }

    /**
     * @return string
     */
    public function getResource(): string
    {
        return $this->resource;
    }

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->api_key;
    }

    /**
     * @return array
     */
    public function format(): array
    {
        return [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'X-Access' => $this->getAccessToken(),
            'X-Resource' => $this->getResource(),
            'X-API-Key' => $this->getApiKey(),
        ];
    }
}