<?php

use ArmourDev\EposSDK\Domain\Entity\Address;

use function Pest\Faker\faker;

it('should create an address', function () {
    // Arrange.
    $address_line_1 = faker()->streetAddress();
    $address_line_2 = faker()->word();
    $city = faker()->city();
    $postcode = faker()->postcode();

    // Act.
    $address = Address::create(
        $address_line_1,
        $address_line_2,
        $city,
        $postcode,
    );

    // Assert.
    expect($address)->toBeObject()
        ->and($address->getAddressLine1())->toBeString()->toBe($address_line_1)
        ->and($address->getAddressLine2())->toBeString()->toBe($address_line_2)
        ->and($address->getCity())->toBeString()->toBe($city)
        ->and($address->getPostcode())->toBeString()->toBe($postcode);
});

it('should return address formatted', function () {
    // Arrange.
    $address = createAddress();

    // Act.
    $formatted = $address->format();

    // Assert.
    expect($formatted)->toBeArray()->toHaveCount(4)->toHaveKeys([
        'address_line_1',
        'address_line_2',
        'city',
        'postcode',
    ]);
});
