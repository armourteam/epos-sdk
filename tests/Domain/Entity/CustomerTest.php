<?php

use ArmourDev\EposSDK\Domain\Entity\Customer;

use function Pest\Faker\faker;

it('should create a customer', function () {
    $first_name = faker()->firstName();
    $last_name = faker()->lastName();
    $email = faker()->safeEmail();
    $phone = faker()->phoneNumber();

    $customer = Customer::create(
        first_name: $first_name,
        last_name: $last_name,
        email: $email,
        phone: $phone,
    );

    expect($customer)->toBeObject()
        ->and($customer->getFirstName())->toBeString()->toBe($first_name)
        ->and($customer->getLastName())->toBeString()->toBe($last_name)
        ->and($customer->getEmail())->toBeString()->toBe($email)
        ->and($customer->getPhone())->toBeString()->toBe($phone);
});

it('should return customer formatted', function () {
    // Arrange.
    $customer = createCustomer();

    // Act.
    $formatted = $customer->format();

    // Assert.
    expect($formatted)->toBeArray()->toHaveCount(4)->toHaveKeys([
        'first_name',
        'last_name',
        'email',
        'phone',
    ]);
});
