<?php

use ArmourDev\EposSDK\Domain\Entity\Discount;

use function Pest\Faker\faker;

it('should create a discount', function () {
    $description = faker()->words(3, true);
    $total = faker()->randomFloat(2, 0, 10);

    $discount = Discount::create(
        description: $description,
        total: $total,
    );

    expect($discount)->toBeObject()
        ->and($discount->getDescription())->toBeString()->toBe($description)
        ->and($discount->getTotal())->toBeInt()->toBe(intval($total * 100));
});

it('should return a discount formatted', function () {
    // Arrange.
    $discount = createDiscount();

    // Act.
    $formatted = $discount->format();

    // Assert.
    expect($formatted)->toBeArray()->toHaveCount(2)->toHaveKeys([
        'description',
        'total',
    ]);
});
