<?php

use ArmourDev\EposSDK\Domain\Entity\Fee;
use function Pest\Faker\faker;

it('should create a fee', function () {
    $name = faker()->words(3, true);
    $price = faker()->randomFloat(2, 0, 10);
    $ref = faker()->word();

    $fee = Fee::create(
        name: $name,
        price: $price,
        ref: $ref
    );

    expect($fee)->toBeObject()
        ->and($fee->getName())->toBeString()->toBe($name)
        ->and($fee->getPrice())->toBeInt()->toBe(intval($price * 100))
        ->and($fee->getRef())->toBeString()->toBe($ref);
});

it('should create a fee with ref as null if was passed an empty string', function () {
    $name = faker()->words(3, true);
    $price = faker()->randomFloat(2, 0, 10);

    $fee = Fee::create(
        name: $name,
        price: $price,
        ref: ''
    );

    expect($fee)->toBeObject()
        ->and($fee->getName())->toBeString()->toBe($name)
        ->and($fee->getPrice())->toBeInt()->toBe(intval($price * 100))
        ->and($fee->getRef())->toBeNull();
});

it('should create a fee with ref as null if ref was not passed', function () {
    $name = faker()->words(3, true);
    $price = faker()->randomFloat(2, 0, 10);

    $attributes = [
        'name' => $name,
        'price' => $price,
    ];

    $fee = Fee::create(
        name: $name,
        price: $price,
    );

    expect($fee)->toBeObject()
        ->and($fee->getName())->toBeString()->toBe($name)
        ->and($fee->getPrice())->toBeInt()->toBe(intval($price * 100))
        ->and($fee->getRef())->toBeNull();
});

it('should return a fee formatted', function () {
    // Arrange.
    $name = faker()->words(3, true);
    $price = faker()->randomFloat(2, 0, 10);
    $ref = faker()->word();

    $attributes = [
        'name' => $name,
        'price' => $price,
        'ref' => $ref,
    ];

    $fee = Fee::create(
        name: $name,
        price: $price,
        ref: $ref
    );

    // Act.
    $formatted = $fee->format();

    // Assert.
    expect($formatted)->toBeArray()->toHaveCount(3)->toHaveKeys([
        'name',
        'price',
        'ref',
    ]);
});
