<?php

use ArmourDev\EposSDK\Domain\Entity\Item;

use function Pest\Faker\faker;

it('should create an item', function () {
    $id = faker()->randomNumber(1);
    $name = faker()->word();
    $quantity = faker()->randomNumber(1);
    $price = faker()->randomFloat(2, 0, 10);

    $item = Item::create(
        id: $id,
        name: $name,
        quantity: $quantity,
        price: $price,
    );

    expect($item)->toBeObject()
        ->and($item->getId())->toBeInt()->toBe($id)
        ->and($item->getName())->toBeString()->toBe($name)
        ->and($item->getQuantity())->toBeInt()->toBe($quantity)
        ->and($item->getPrice())->toBeInt()->toBe(intval($price * 100));
});

it('should create an item with comments', function () {
    $id = faker()->randomNumber(1);
    $name = faker()->word();
    $quantity = faker()->randomNumber(1);
    $price = faker()->randomFloat(2, 0, 10);

    $item = Item::create(
        id: $id,
        name: $name,
        quantity: $quantity,
        price: $price,
        comments: [faker()->word(), faker()->word()]
    );

    expect($item)->toBeObject()
        ->and($item->getNotes())->toBeArray()->toHaveCount(2);

    $item = $item->format();

    expect($item)->toBeArray()->toHaveKey('notes');
});

it('should add items to the item', function () {
    // Arrange.
    $item = createItem();

    // Act.
    $item->addItems(...createItems(3));

    // Assert.
    expect($item->getItems())->toHaveCount(3);
});


it('should add sub items into item', function () {
    // Arrange.
    $id = faker()->randomNumber(1);
    $name = faker()->word();
    $quantity = faker()->randomNumber(1);
    $price = faker()->randomFloat(2, 0, 10);

    // Act.
    $item = Item::create(
        id: $id,
        name: $name,
        quantity: $quantity,
        price: $price,
    );

    $item->addItems(...createItems(2));

    // Assert.
    expect($item->getItems())->toHaveCount(2);
});

it('should return an item formatted', function () {
    // Arrange.
    $item = createItem();

    // Act.
    $formatted = $item->format();

    // Assert.
    expect($formatted)->toBeArray()->toHaveCount(4)->toHaveKeys([
        'id',
        'name',
        'quantity',
        'price',
    ]);
});

it('should return an item with sub items formatted', function () {
    // Arrange.
    $item = createItem();

    // Act.
    $item->addItems(...createItems(3));
    $formatted = $item->format();

    // Assert.
    expect($formatted)->toBeArray()->toHaveCount(5)->toHaveKeys([
        'id',
        'name',
        'quantity',
        'price',
        'items',
    ]);
});
