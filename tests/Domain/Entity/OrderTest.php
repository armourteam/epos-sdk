<?php

use ArmourDev\EposSDK\Domain\Entity\Address;
use ArmourDev\EposSDK\Domain\Entity\Customer;
use ArmourDev\EposSDK\Domain\Entity\Order;
use ArmourDev\EposSDK\Domain\Entity\OrderType;
use ArmourDev\EposSDK\Domain\Entity\Payment;

use function Pest\Faker\faker;

it('should create an order', function () {
    $id = faker()->randomNumber(1);
    $restaurant_id = faker()->randomNumber(1);
    $epos_restaurant_id = (string) faker()->randomNumber(1);
    $ready_at = faker()->date();

    $order = Order::create(
        id: $id,
        restaurant_id: $restaurant_id,
        epos_restaurant_id: $epos_restaurant_id,
        ready_at: $ready_at,
    );

    expect($order)->toBeObject()->toBeInstanceOf(Order::class)
        ->and($order->getId())->toBeInt()->toBe($id)
        ->and($order->getRestaurantId())->toBeInt()->toBe($restaurant_id)
        ->and($order->getEposRestaurantId())->toBeString()->toBe($epos_restaurant_id)
        ->and($order->getReadyAt())->toBeString()->toBe($ready_at);
});

it('should create an order with notes', function () {
    // Arrange.
    $order = createOrder();

    // Act.
    $order->addCustomerNotes([
        ['text' => faker()->word()],
        ['text' => faker()->word()],
        ['text' => faker()->word()],
    ]);

    // Assert.
    expect($order->getCustomerNotes())->toBeArray()->toHaveCount(3);
});

it('should add type to the order', function () {
    $order = createOrder();
    $type = createOrderType();

    $order->addOrderType($type);

    expect($order->getOrderType())->toBeObject()->toBeInstanceOf(OrderType::class);
});

it('should add payment to the order', function () {
    $order = createOrder();
    $payment = createPayment();

    $order->addPayment($payment);

    expect($order->getPayment())->toBeObject()->toBeInstanceOf(Payment::class);
});

it('should add a customer to the order', function () {
    $order = createOrder();
    $customer = createCustomer();

    $order->addCustomer($customer);

    expect($order->getCustomer())->toBeObject()->toBeInstanceOf(Customer::class);
});

it('should add an address to the order', function () {
    $order = createOrder();
    $address = createAddress();

    $order->addAddress($address);

    expect($order->getAddress())->toBeObject()->toBeInstanceOf(Address::class);
});

it('should add discounts to the order', function () {
    $order = createOrder();
    $discount = createDiscount();
    $another_discount = createDiscount();

    $order->addDiscounts($discount, $another_discount);

    expect($order->getDiscounts())->toBeArray()->toHaveCount(2);

    $order->addDiscounts($discount, $another_discount);

    expect($order->getDiscounts())->toBeArray()->toHaveCount(4);
});

it('should add items to the order', function () {
    // Arrange.
    $order = createOrder();
    $quantityOfItems = faker()->randomNumber(1);
    $items = createItems($quantityOfItems);

    // Act.
    $order->addItems(...$items);

    // Assert.
    expect($order->getItems())->toBeArray()->toHaveCount($quantityOfItems);
});

it('should return a order formatted', function () {
    // Arrange.
    $order = createOrder();
    $order->addOrderType(createOrderType());
    $order->addAddress(createAddress());
    $order->addCustomer(createCustomer());
    $order->addDiscounts(createDiscount(), createDiscount());
    $order->addItems(...createItems(3));
    $order->addPayment(createPayment());

    // Act.
    $formatted = $order->format();

    // Assert.
    expect($formatted)->toBeArray()->toHaveCount(1)->toHaveKey('order')
        ->and($formatted['order'])->toBeArray()->toHaveCount(11)->toHaveKeys([
            'id',
            'restaurant_id',
            'epos_restaurant_id',
            'ready_at',
            'type',
            'items',
            'customer',
            'address',
            'payment',
            'discounts',
            'customer_notes',
    ]);
});

it('should throw an error if order has no type', function () {
    // Arrange.
    $order = createOrder();

    // Act.
    $order->validateOrderType();
})->throws(Exception::class);

it('should throw an error if order has no items', function () {
    // Arrange.
    $order = createOrder();

    // Act.
    $order->validateOrderItems();
})->throws(Exception::class);

it('should throw an error if order has no customer', function () {
    // Arrange.
    $order = createOrder();

    // Act.
    $order->validateOrderCustomer();
})->throws(Exception::class);

it('should throw an error if order has no address', function () {
    // Arrange.
    $order = createOrder();

    // Act.
    $order->validateOrderAddress();
})->throws(Exception::class);

it('should throw an error if order has no payment', function () {
    // Arrange.
    $order = createOrder();

    // Act.
    $order->validateOrderPayment();
})->throws(Exception::class);

it('should throw an error if order has no discounts', function () {
    // Arrange.
    $order = createOrder();

    // Act.
    $order->validateOrderDiscounts();
})->throws(Exception::class);

it('should not throw an error if the order has all the information', function () {
    // Arrange.
    $order = createOrder();
    $order->addOrderType(createOrderType());
    $order->addAddress(createAddress());
    $order->addCustomer(createCustomer());
    $order->addDiscounts(createDiscount(), createDiscount());
    $order->addItems(...createItems(3));
    $order->addPayment(createPayment());

    // Assert.
    expect($order->validate())->not()->toThrow(Exception::class);
});