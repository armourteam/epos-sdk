<?php

use ArmourDev\EposSDK\Domain\Entity\OrderType;
use ArmourDev\EposSDK\Domain\Enum\OrderTypeEnum;

use function Pest\Faker\faker;

it('should create an order type (delivery, collection)', function ($description) {
    $ref = faker()->word();
    $table_number = (string) faker()->randomNumber(2);

    $orderType = OrderType::create(
        description: $description,
        ref: $ref,
        table_number: $table_number,
    );

    expect($orderType)->toBeObject()
        ->and($orderType->getDescription())->toBeString()->toBe($description)
        ->and($orderType->getRef())->toBeString()->toBe($ref)
        ->and($orderType->getTableNumber())->toBeNull();
})->with([
    OrderTypeEnum::DELIVERY,
    OrderTypeEnum::COLLECTION,
]);

it('should create an order type (table)', function () {
    $description = 'table';
    $ref = faker()->word();
    $table_number = (string) faker()->randomNumber(2);

    $orderType = OrderType::create(
        description: $description,
        ref: $ref,
        table_number: $table_number,
    );

    expect($orderType)->toBeObject()
        ->and($orderType->getDescription())->toBeString()->toBe($description)
        ->and($orderType->getRef())->toBeString()->toBe($ref)
        ->and($orderType->getTableNumber())->not()->toBeNull()->toBeString()->toBe($table_number);
});

it('should create an order type (delivery, collection) if ref was not passed', function ($description) {
    $orderType = OrderType::create(
        description: $description
    );

    expect($orderType)->toBeObject()
        ->and($orderType->getDescription())->toBeString()->toBe($description)
        ->and($orderType->getRef())->toBeNull()
        ->and($orderType->getTableNumber())->toBeNull();
})->with([
    OrderTypeEnum::DELIVERY,
    OrderTypeEnum::COLLECTION,
]);

it('should throw an exception if description was table but was not passed the table number', function () {
    $description = 'table';
    $ref = faker()->word();

    OrderType::create(
        description: $description,
        ref: $ref
    );
})->throws(Exception::class, 'table number should be defined');

it('should return a order type formatted', function () {
    // Arrange.
    $orderType = createOrderType();

    // Act.
    $formatted = $orderType->format();

    // Assert.
    expect($formatted)->toBeArray()->toHaveCount(3)->toHaveKeys([
        'description',
        'ref',
        'table_number',
    ]);
});
