<?php

use ArmourDev\EposSDK\Domain\Entity\Fee;
use ArmourDev\EposSDK\Domain\Entity\Payment;

use function Pest\Faker\faker;

it('should create a payment', function () {
    $total = faker()->randomFloat(2, 0, 10);
    $status = faker()->boolean();
    $method = faker()->word();

    $payment = Payment::create(
        total: $total,
        status: $status,
        method: $method,
    );

    expect($payment)->toBeObject()->toBeInstanceOf(Payment::class)
        ->and($payment->getTotal())->toBeInt()->toBe(intval($total * 100))
        ->and($payment->getStatus())->toBeBool()->toBe($status)
        ->and($payment->getMethod())->toBeString()->toBe($method);
});

it('should add fees inside a payment', function () {
    $payment = createPayment();

    $payment->setFees(
        Fee::create(
            name: faker()->words(3, true),
            price: faker()->randomFloat(2, 0, 10),
            ref: faker()->word(),
        ),
        Fee::create(
            name: faker()->words(3, true),
            price: faker()->randomFloat(2, 0, 10),
            ref: faker()->word(),
        ),
    );

    expect($payment->getFees())->toBeArray()->toHaveCount(2);
});

it('should return a payment formatted without fees', function () {
    // Arrange.
    $total = faker()->randomFloat(2, 0, 10);
    $status = faker()->boolean();
    $method = faker()->word();

    $payment = Payment::create(
        total: $total,
        status: $status,
        method: $method,
    );

    // Act.
    $formatted = $payment->format();

    // Assert.
    expect($formatted)->toBeArray()->toHaveCount(3)->toHaveKeys([
        'total',
        'status',
        'method',
    ]);
});

it('should return a payment formatted with fees', function () {
    // Arrange.
    $payment = createPayment();

    $payment->setFees(
        Fee::create(
            name: faker()->words(3, true),
            price: faker()->randomFloat(2, 0, 10),
            ref: faker()->word(),
        ),
        Fee::create(
            name: faker()->words(3, true),
            price: faker()->randomFloat(2, 0, 10),
            ref: faker()->word(),
        ),
    );

    // Act.
    $formatted = $payment->format();

    // Assert.
    expect($formatted)->toBeArray()->toHaveCount(4)->toHaveKeys([
        'total',
        'status',
        'method',
        'fees',
    ]);
});
