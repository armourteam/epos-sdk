<?php

use ArmourDev\EposSDK\Infra\Http\Api;
use ArmourDev\EposSDK\Infra\Http\Header;

use function Pest\Faker\faker;

it('should return an api instance', function () {
    $header = Header::create(
        access_token: faker()->word(),
        resource: faker()->word(),
        api_key: faker()->word()
    );

    $api = new Api(
        apiUrl: "http://epos.armour.test",
        header: $header,
    );

    expect($api)->toBeInstanceOf(Api::class);
})->group('integration');
