<?php

use ArmourDev\EposSDK\Infra\Http\Header;
use function Pest\Faker\faker;

it('should return a new instance of header', function () {
   $header = Header::create(
       access_token: faker()->word(),
       resource: faker()->word(),
       api_key: faker()->word(),
   );

   expect($header)->toBeInstanceOf(Header::class);
})->group("integration");
