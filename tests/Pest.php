<?php

use ArmourDev\EposSDK\Domain\Entity\Address;
use ArmourDev\EposSDK\Domain\Entity\Customer;
use ArmourDev\EposSDK\Domain\Entity\Discount;
use ArmourDev\EposSDK\Domain\Entity\Item;
use ArmourDev\EposSDK\Domain\Entity\Order;
use ArmourDev\EposSDK\Domain\Entity\OrderType;
use ArmourDev\EposSDK\Domain\Entity\Payment;

use function Pest\Faker\faker;

function createOrder(): Order
{
    $id = faker()->randomNumber(1);
    $restaurant_id = faker()->randomNumber(1);
    $epos_restaurant_id = (string) faker()->randomNumber(1);
    $ready_at = faker()->date();

    return Order::create(
        id: $id,
        restaurant_id: $restaurant_id,
        epos_restaurant_id: $epos_restaurant_id,
        ready_at: $ready_at,
    );
}

function createOrderType(): OrderType
{
    $description = faker()->word();
    $ref = faker()->word();
    $table_number = (string) faker()->randomNumber(2);

    return OrderType::create(
        description: $description,
        ref: $ref,
        table_number: $table_number,
    );
}

function createPayment(): Payment
{
    $total = faker()->randomFloat(2, 0, 10);
    $status = faker()->boolean();
    $method = faker()->word();

    return Payment::create(
        total: $total,
        status: $status,
        method: $method,
    );
}

function createCustomer(): Customer
{
    $first_name = faker()->firstName();
    $last_name = faker()->lastName();
    $email = faker()->safeEmail();
    $phone = faker()->phoneNumber();

    return Customer::create(
        first_name: $first_name,
        last_name: $last_name,
        email: $email,
        phone: $phone,
    );
}

function createAddress(): Address
{
    $address_line_1 = faker()->streetAddress();
    $address_line_2 = faker()->word();
    $city = faker()->city();
    $postcode = faker()->postcode();

    return Address::create(
        $address_line_1,
        $address_line_2,
        $city,
        $postcode
    );
}

function createDiscount(): Discount
{
    $description = faker()->words(3, true);
    $total = faker()->randomFloat(2, 0, 10);

    return Discount::create(
        description: $description,
        total: $total,
    );
}

function createItem(): Item
{
    $id = faker()->randomNumber(1);
    $name = faker()->word();
    $quantity = faker()->randomNumber(1);
    $price = faker()->randomFloat(2, 0, 10);

    return Item::create(
        id: $id,
        name: $name,
        quantity: $quantity,
        price: $price,
    );
}

function createItems(int $quantity = 2): array
{
    $items = [];

    for ($i = 0; $i < $quantity; $i++) {
        $items[] = createItem();
    }

    return $items;
}